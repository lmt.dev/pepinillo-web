package com.hombrepai.pepinillomvc.controller;

import com.hombrepai.pepinillomvc.model.Administrator;
import com.hombrepai.pepinillomvc.model.Player;
import com.hombrepai.pepinillomvc.repository.PlayerRepository;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



@Controller
public class PlayerController {


    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    public void setPlayerRepository(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @RequestMapping(path = "/players/add", method = RequestMethod.GET)
    public String createPlayer(Model model, HttpSession session) {
        boolean inSession =
            (boolean)
                session.getAttribute("inSession");
        
        if (inSession){            
        
            Administrator administrator =
                (Administrator)session.getAttribute("administrator");
            
            Player player = new Player();        
            player.setAdministrator(administrator.getUser());
            
            session.setAttribute("player", player);
            session.setAttribute("inSession", inSession);
            
            model.addAttribute("player", player);
            model.addAttribute("inSession", inSession);
            
            return "edit";
        }
        else{
            return "logout";
        }
    }

    @RequestMapping(path = "players", method = RequestMethod.POST)
    public String savePlayer(Player player, HttpSession session) {
        
        boolean inSession =
            (boolean)
                session.getAttribute("inSession");
        
        if (inSession){            
            Administrator administrator =
                (Administrator)session.getAttribute("administrator");
            player.setAdministrator(administrator.getUser());

            playerRepository.save(player);
            return "redirect:/players";
        }
        else{
            return "logout";
        }
    }

    @RequestMapping(path = "/players/edit/{id}", method = RequestMethod.GET)
    public String editPlayer(
            Model model,
            @PathVariable(value = "id") String id,
            HttpSession session
    ) {        
        boolean inSession =
            (boolean)
                session.getAttribute("inSession");
        
        if (inSession){            
        
            Administrator administrator =
                (Administrator)session.getAttribute("administrator");
            //System.out.println(administrator.getUser()+" "+administrator.getPassword());
            List<Player> players = playerRepository.findByAdministrator(administrator.getUser());

            model.addAttribute("player", playerRepository.findOne(id));
            model.addAttribute("inSession", inSession);
            
            return "edit";
        }
        else{
            return "logout";
        }
    }

    @RequestMapping(path = "/players/delete/{id}", method = RequestMethod.GET)
    public String deletePlayer(
            @PathVariable(name = "id") String id
    ) {
        Player player = playerRepository.findOne(id);
        player.setEliminado(true);
        playerRepository.save(player);
        return "redirect:/players";
    }
    
    @RequestMapping(path = "/players/reactive/{id}", method = RequestMethod.GET)
    public String reactivePlayer(
            @PathVariable(name = "id") String id
    ) {
        Player player = playerRepository.findOne(id);
        player.setEliminado(false);
        playerRepository.save(player);
        return "redirect:/players";
    }
    
    @RequestMapping(path = "players", method = RequestMethod.GET)
    public String getPlayers(
            Model model,
            HttpSession session
    ) {
        boolean inSession =
            (boolean)session.getAttribute("inSession");
        
       if(inSession){
            Administrator administrator =
                (Administrator)session.getAttribute("administrator");

            List<Player> players = 
                    playerRepository.
                            findByAdministrator(administrator.getUser());
           
            session.setAttribute("players", players);
            session.setAttribute("administrator", administrator);
            session.setAttribute("inSession", inSession);

            model.addAttribute("players", players);
            model.addAttribute("administrator", administrator);
            model.addAttribute("inSession", inSession);
            
            return "players";
       }
       else{
           return "logout";
       }
        
        
        
    }
}
