package com.hombrepai.pepinillomvc.repository;

import com.hombrepai.pepinillomvc.model.Administrator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, String> {
    public Administrator findByUser(String user);
}
