package com.hombrepai.pepinillomvc.repository;

import com.hombrepai.pepinillomvc.model.Player;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends JpaRepository<Player, String> {
    public List<Player> findByAdministrator(String administrator);
}